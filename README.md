# remix-course

## Getting started

to create your remix project, execute:

```bash
npx create-remix@latest
```

## Run docker database (Prisma is not enable for local mongo DB)

```bash
# start the mongo instance
sudo docker-compose up -d
# stop the mongo instance
sudo docker-compose down
# kill all instances of docker
sudo docker system prune -a
sudo docker rm -f $(sudo docker ps -a -q)
#remove volumes
sudo docker volume rm $(sudo docker volume ls -q)
```