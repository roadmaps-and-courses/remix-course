import { Form, Link, useActionData, useNavigation, useSearchParams } from '@remix-run/react';
import { FaLock, FaUserPlus } from 'react-icons/fa';

function AuthForm() {

  const [searchParams] = useSearchParams()
  const navigation = useNavigation()
  const validationErrors = useActionData()

  const authMode = (searchParams.get('mode') ?? 'login') === 'login'
  const btnCaption = authMode ? 'Login' : 'Create User'
  const toggleBtn = authMode ? 'Create New User' : 'Log in with existing user'
  const isSubmiting = navigation.state !== 'idle'

  return (
    <Form method="post" className="form" id="auth-form">
      <div className="icon-img">
        { authMode ? <FaLock /> : <FaUserPlus /> }
      </div>
      <p>
        <label htmlFor="email">Email Address</label>
        <input type="email" id="email" name="email" required />
      </p>
      <p>
        <label htmlFor="password">Password</label>
        <input type="password" id="password" name="password" minLength={7} />
      </p>
      {validationErrors && <ul>{
        typeof validationErrors === "string" ? <li>{validationErrors}</li> :
        Object.values(validationErrors).map(err => <li key={err as string}>{err as string}</li>)
      }</ul>}
      <div className="form-actions">
        <button disabled={isSubmiting}>{isSubmiting ? 'Authenticating...' : btnCaption}</button>
        <Link to={`?mode=${authMode ? 'signup' : 'login'}`}>{toggleBtn}</Link>
      </div>
    </Form>
  );
}

export default AuthForm;
