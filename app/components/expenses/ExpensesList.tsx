import ExpenseListItem from './ExpenseListItem';

interface ExpenseListProps {
  expenses: {
    id: any,
    title: string,
    amount: number
  }[];
}


function ExpensesList({ expenses }: ExpenseListProps) {

  return (
    <ol id="expenses-list">
      {expenses.map((expense) => (
        <li key={expense.id}>
          <ExpenseListItem
            id={expense.id}
            title={expense.title}
            amount={expense.amount}
          />
        </li>
      ))}
    </ol>
  );
}

export default ExpensesList;
