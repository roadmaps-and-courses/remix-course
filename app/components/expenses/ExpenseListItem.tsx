import { Form, Link, useFetcher } from "@remix-run/react";

interface ExpenseListProps {
  id?: any,
  title: string,
  amount: number
}


function ExpenseListItem({ id, title, amount }: ExpenseListProps) {
  /* const fetch = useFetcher(); */
  /* function deleteExpenseItemHandler() {
    const confirmation = confirm("Are you sure?")
    if(confirmation) {
      fetch.submit(null, {method: 'delete', action: `/expenses/${id}`})
    }
    return
  } */

  /* if(fetch.state !== 'idle') {
    return <article className="expense-item locked" >
      <p>deleting...</p>
    </article>
  } */

  return (
    <article className="expense-item">
      <div>
        <h2 className="expense-title">{title}</h2>
        <p className="expense-amount">${amount.toFixed(2)}</p>
      </div>
      <menu className="expense-actions">
        <Form method="delete" action={`/expenses/${id}`}>
          <button /* onClick={deleteExpenseItemHandler} */>Delete</button>
        </Form>
        <Link to={id}>Edit</Link>
      </menu>
    </article>
  );
}

export default ExpenseListItem;
