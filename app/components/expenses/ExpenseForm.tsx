import { Form, Link, useActionData, useLoaderData, useMatches, useNavigation, useParams, useSubmit } from "@remix-run/react";

function ExpenseForm() {
  const today = new Date().toISOString().slice(0, 10); // yields something like 2023-09-10
  const validationErrors  = useActionData();
  const navigation = useNavigation();
  const params = useParams();
  /* const expense = useLoaderData(); */
  const matches = useMatches()

  const expense = params?.id ? (
    (matches.find(match => match.id === 'routes/_exp'))?.data
  ).find(
    (exp: any) => exp.id === params.id
  ) : null

  if(params.id && !expense) return <p>Invalid exponse ID</p>

  const defaultValues = expense ?  {
    title: expense.title,
    amount: expense.amount,
    date: expense.date
  } : {
    title: '',
    amount: '',
    date: ''
  }

  const isSubmitting = navigation.state !== 'idle'

  //const submit = useSubmit();

  /*const submitHandler = (event: SubmitEvent) => {
    event.preventDefault()
    //validation was here
    submit(event.target, {
      //action: 'expenses/add',
      method: 'post'
    })
  }*/

  return (
    <Form
      method={expense ? "patch" : "post"}
      className="form"
      id="expense-form"
      // onSubmit={submitHandler}
      >

      <p>
        <label htmlFor="title">Expense Title</label>
        <input type="text" id="title" name="title" required maxLength={30} defaultValue={defaultValues.title} />
      </p>

      <div className="form-row">
        <p>
          <label htmlFor="amount">Amount</label>
          <input
            type="number"
            id="amount"
            name="amount"
            min="0"
            step="0.01"
            defaultValue={defaultValues.amount}
            required
          />
        </p>
        <p>
          <label htmlFor="date">Date</label>
          <input type="date" id="date" name="date" max={today} required defaultValue={defaultValues.date ? defaultValues.date.slice(0,10) : ''}/>
        </p>
      </div>
      {validationErrors && <ul>
        {Object.values(validationErrors as string[]).map(err => <li key={err}>
          {err}
        </li>)}
      </ul>}
      <div className="form-actions">
        <button disabled={isSubmitting}>
          {isSubmitting ? 'Saving...' : 'Save Expense'}
        </button>
        <Link to="..">Cancel</Link>
      </div>
    </Form>
  );
}

export default ExpenseForm;
