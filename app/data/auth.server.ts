import { createCookieSessionStorage, redirect } from "@remix-run/node";
import { prisma } from "./database.server";
import { compare, hash } from "bcryptjs";

const SESSION_SECRET: string = process.env.SESSION_SECRET || ''

const sessionStorage = createCookieSessionStorage({
  cookie: {
    secure: process.env.NODE_ENV === "production",
    secrets: [SESSION_SECRET],
    sameSite: "lax",
    maxAge: 30*24*60*60,
    httpOnly: true
  }
})

export async function createUserSession(userId: string, redidrectPath: string) {
  const session = await sessionStorage.getSession()
  session.set('userId', userId)
  return redirect(redidrectPath, {
    headers: {
      'Set-Cookie': await sessionStorage.commitSession(session)
    }
  })
}

export async function getUserSession(request: Request) {
  const session  = await sessionStorage.getSession(request.headers.get('Cookie'))
  const userId = session.get('userId')
  if(!userId) {
    return null
  }
  return userId;
}

export async function signup({email, password}: any) {
  const existingUser = await prisma.user.findFirst({where: {email}});
  if(existingUser) {
    throw new Response('A user with the provided email already exists...', {status: 422})
  }
  const hashedPassword = await hash(password,12)
  const user = await prisma.user.create({data: {
    email,
    password: hashedPassword
  }})
  return createUserSession(user.id,'/expenses')
}

export async function login({email, password}: any) {
  const existingUser = await prisma.user.findFirst({where: {email}});
  if(!existingUser || !(await compare(password, existingUser?.password))) {
    throw new Response("Could not log you in, please check your credentials...", {status: 401})
  }
  return createUserSession(existingUser.id,'/expenses')
}

export async function  logout(request: Request, redidrectPath: string){
  const session  = await sessionStorage.getSession(request.headers.get('Cookie'))
  return redirect(redidrectPath, {
    headers: {
      'Set-Cookie': await sessionStorage.destroySession(session)
    }
  })
}

export async function requireUserSession(request: Request){
  const userId = await getUserSession(request);
  if(!userId) {
    throw redirect('/auth')
  }
  return userId
}