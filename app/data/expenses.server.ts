import { prisma } from "./database.server"

export async function addexpense({title, amount, date}: any, userId: string) {
  try {
    return await prisma.expense.create({data: {
      title,
      amount: +amount,
      date: new Date(date),
      User: {
        connect: {id: userId}
      }
    }})
  } catch(err) {
    console.error(err)
    throw err
  }
}

export async function getExpenses(userId: string) {
  try {
    const expenses = await prisma.expense.findMany({where: {userId}, orderBy: {date: 'desc'}});
    return expenses;
  }catch (err) {
    console.error(err)
    throw err
  }
}

export async function getExpenseByID(id: string | undefined) {
  try {
    const expense = await prisma.expense.findFirst({where: {id}});
    return expense;
  }catch (err) {
    console.error(err)
    throw err
  }
}

export async function updateExpense(id: string | undefined, {title, amount, date}: any) {
  try {
    await prisma.expense.update({
      where: {id},
      data: {
        title,
        amount: +amount,
        date: new Date(date)
      }
    })
  }catch (err) {
    console.error(err)
    throw err
  }
}

export async function deleteExpense(id: string | undefined) {
  try {
    await prisma.expense.delete({where: {id}})
  }catch (err) {
    console.error(err)
    throw err
  }
}