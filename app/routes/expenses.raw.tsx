import { LoaderArgs } from "@remix-run/node";
import type { V2_MetaFunction } from "@remix-run/react";
import { requireUserSession } from "~/data/auth.server";
import { getExpenses } from "~/data/expenses.server";

export const meta: V2_MetaFunction = () => {
  return [{ title: "ExpensesRaw" }];
};

export async function loader({request}: LoaderArgs) {
  //remember to add this to all of your protected loaders
  const userId = await requireUserSession(request)
  return getExpenses(userId)
}