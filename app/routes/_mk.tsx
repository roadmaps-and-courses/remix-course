import { ActionArgs } from "@remix-run/node";
import { Outlet } from "@remix-run/react";
import MainHeader from "~/components/navigation/MainHeader";
import { getUserSession } from "~/data/auth.server";

import styles from "~/styles/marketing.css"

export default function Expenses() {
  return <>
    <MainHeader />
    <Outlet/>
  </>;
}

export function links() {
  return [{rel: 'stylesheet', href: styles}]
}

export function loader({request}: ActionArgs) {
  return getUserSession(request)
}