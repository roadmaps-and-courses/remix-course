import { Link, V2_MetaFunction, useMatches } from "@remix-run/react";

import ExpenseStatistics from "~/components/expenses/ExpenseStatistics";
import Chart from "~/components/expenses/Chart";

export const meta: V2_MetaFunction = () => {
  return [{ title: "ExpensesAnalysis" }];
};

export default function ExpensesAnalysis() {
  const matches = useMatches()
  const expenses = (matches.find(match => match.id === 'routes/_exp'))?.data
  return (
    <main>
      {expenses && expenses.length > 0 ? <>
        <Chart expenses={expenses} />
        <ExpenseStatistics expenses={expenses} />
      </> : <section id="no-expenses">
        <h1>No expenses found</h1>
        <p>Start <Link to="/expenses/add">adding some</Link> today.</p>
      </section>}

    </main>
  );
}