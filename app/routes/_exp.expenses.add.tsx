import { ActionArgs, redirect } from "@remix-run/node";
import { V2_MetaFunction, useNavigate } from "@remix-run/react";
import ExpenseForm from "~/components/expenses/ExpenseForm";
import Modal from "~/components/util/Modal";
import { requireUserSession } from "~/data/auth.server";
import { addexpense } from "~/data/expenses.server";
import { validateExpenseInput } from "~/data/validators.server";

export const meta: V2_MetaFunction = () => {
  return [{ title: "ExpensesAdd" }];
};

export default function ExpensesAdd() {
  const navigate = useNavigate();

  const closeHandler = () => {
    navigate('..')
  }

  return (
    <Modal onClose={closeHandler}>
      <ExpenseForm/>
    </Modal>
  );
}

export async function action({request} : ActionArgs) {
  const userId = await requireUserSession(request)
  const formData = await request.formData();
  const data= Object.fromEntries(formData)

  try {
    validateExpenseInput(data)
  } catch(err) {
    return err
  }

  await addexpense(data,userId)
  return redirect('..')
}