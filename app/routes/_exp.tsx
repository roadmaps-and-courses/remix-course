import { LoaderArgs } from "@remix-run/node";
import { Outlet } from "@remix-run/react";
import ExpensesHeader from "~/components/navigation/ExpenseesHeader";
import { requireUserSession } from "~/data/auth.server";
import { getExpenses } from "~/data/expenses.server";

import styles from "~/styles/expenses.css"

export default function Expenses() {
  return <>
    <ExpensesHeader/>
    <Outlet/>
  </>;
}

export function links() {
  return [{rel: 'stylesheet', href: styles}]
}

export async function loader({request}: LoaderArgs) {
  //remember to add this to all of your protected loaders
  const userId = await requireUserSession(request)
  return getExpenses(userId)
}