import { ActionArgs, LoaderArgs, redirect } from "@remix-run/node";
import { V2_MetaFunction, useLoaderData, useNavigate } from "@remix-run/react";
import ExpenseForm from "~/components/expenses/ExpenseForm";
import Modal from "~/components/util/Modal";
import { deleteExpense, updateExpense } from "~/data/expenses.server";
import { validateExpenseInput } from "~/data/validators.server";
/* import { getExpenseByID } from "~/data/expenses.server"; */

export const meta: V2_MetaFunction = ({params, location, data/*, parentsData*/}) => {
  return [{ title: "ExpensesID" }];
};

export default function ExpensesID() {
  const navigate = useNavigate();

  const closeHandler = () => {
    navigate('..')
  }

  return (
    <Modal onClose={closeHandler}>
      <ExpenseForm/>
    </Modal>
  );
}

/*export function loader({params}: LoaderArgs) {
  //remember to add this to all of your protected loaders
  await requireUserSession(request)
  const expenseId = params?.id;
  return getExpenseByID(expenseId)
}*/

export async function action({params, request}: ActionArgs) {
  const expenseId = params.id;
  switch (request.method) {
    case "DELETE":
      await deleteExpense(expenseId)
      return redirect('/expenses')
      break;
    case "PATCH":
      const formData = await request.formData();
      const data = Object.fromEntries(formData)
      try {
        validateExpenseInput(data)
      } catch(err) {
        return err
      }
      await updateExpense(expenseId, data)
      return redirect('..')
    default:
      break;
  }


}