import { Link, Outlet, V2_MetaFunction, useLoaderData, useMatches } from "@remix-run/react";
import { FaDownload, FaPlus } from "react-icons/fa";
import ExpensesList from "~/components/expenses/ExpensesList";


export const meta: V2_MetaFunction = () => {
  return [{ title: "Expenses" }];
};

export default function Expenses() {

  const matches = useMatches()
  const expenses = (matches.find(match => match.id === 'routes/_exp'))?.data

  const hasExpenses = expenses && expenses.length > 0

  return (
    <>
      <Outlet/>
      <main>
        <section id="expenses-actions">
          <Link to="add">
            <FaPlus/>
            <span>Add</span>
          </Link>
          <a href="/expenses/raw">
            <FaDownload/>
            <span>All expenses</span>
          </a>
        </section>
        { hasExpenses ? <ExpensesList expenses={expenses} /> : <section id="no-expenses">
          <h1>No expenses found</h1>
          <p>Start <Link to="/expenses/add">adding some</Link> today.</p>
        </section>}
      </main>
    </>
  );
}