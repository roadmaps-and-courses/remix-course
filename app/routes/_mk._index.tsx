import { FaArrowRight, FaDollarSign, FaChartBar } from 'react-icons/fa';
import type { V2_MetaFunction } from "@remix-run/react";
import { Link } from '@remix-run/react';

export const meta: V2_MetaFunction = () => {
  return [{
    title: "New Remix App",
    desciption: "This is only a example app",
    charSet: "utf-8",
    viewport: "width=device-width,initial-scale=1"
 }];
};

export default function Index() {
  return (
    <main>
      <section className="marketing-section">
        <header>
          <FaDollarSign />
          <h2>A Central Space</h2>
        </header>
        <div className="marketing-content">
          <div className="marketing-image">
            <img src="images/expenses-management.jpg" alt="A list of expenses." className="index-image"/>
          </div>
          <div className="marketing-explanation">
            <p>Manage your expenses in one central place.</p>
            <p>
              <Link className="cta" to="/expenses">
                <span>Get Started</span>
                <FaArrowRight />
              </Link>
            </p>
          </div>
        </div>
      </section>
      <section className="marketing-section">
        <header>
          <FaChartBar />
          <h2>Detailed Analytics</h2>
        </header>
        <div className="marketing-content">
          <p className='marketing-explanation'>
            Benefit from best-in-class analytics to understand your spending
            patterns.
          </p>
          <div className="marketing-image">
            <img src="images/expenses-chart.jpg" alt="A demo bar chart." className="index-image" />
          </div>
        </div>
      </section>
    </main>
  );
}

//only applies on the present, not in childs
export function headers(){
  return {
    'Cache-Control': 'max-age=3600'
  }
}

export const handle = {disableJS: true}