import { ActionArgs, redirect } from "@remix-run/node";
import type { V2_MetaFunction } from "@remix-run/react";

import AuthForm from "~/components/auth/AuthForm"
import { login, signup } from "~/data/auth.server";
import { validateCredentials } from "~/data/validators.server";
import styles from "~/styles/auth.css"


export const meta: V2_MetaFunction = () => {
  return [{ title: "Auth" }];
};

export default function Auth() {
  return (
    <AuthForm/>
  );
}

export function links() {
  return [{rel: 'stylesheet', href: styles}]
}

export async function action({request}: ActionArgs) {
  const searchParams = new URL(request.url).searchParams
  const authMode = (searchParams.get('mode') || 'login')

  const formData = await request.formData()
  const credentials = Object.fromEntries(formData)

  try {
    validateCredentials(credentials)
  } catch(err) {
    return err
  }

  try {
    if(authMode === 'login') {
      return await login(credentials)
    } else {
      return await signup(credentials)
    }
  } catch(err) {
    return err
  }
}