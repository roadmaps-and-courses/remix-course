import { ActionArgs, json } from "@remix-run/node";
import { logout } from "~/data/auth.server";

export async function action({request}: ActionArgs){
  if(request.method !== "POST"){
    throw json({message: "Invalid request methodd"}, {status: 400})
  }
  return logout(request,'/')
}