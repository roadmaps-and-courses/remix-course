import {
  Link,
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  isRouteErrorResponse,
  useMatches,
  useRouteError,
} from "@remix-run/react";

import sharedStyles from '~/styles/shared.css'
import ErrorSection from "./components/util/ErrorSection";

function Document({children}: any ) {
  const matches = useMatches()


  const disableJS = matches.some(match => match.handle?.disableJS)

  return (<html lang="en">
  <head>
    <meta charSet="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <Meta />
    <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;700&display=swap" rel="stylesheet" />
    <Links />
  </head>
  <body>
    {children}
    <ScrollRestoration />
    {!disableJS && <Scripts />}
    <LiveReload />
  </body>
</html>)
}

export default function App() {
  return (
    <Document>
      <Outlet/>
    </Document>
  );
}

export function links() {
  return [{rel: 'stylesheet', href: sharedStyles}]
}

export function ErrorBoundary() {
  let error = useRouteError();

  if (isRouteErrorResponse(error) ) {
    return (
      <Document>
        <main>
          <ErrorSection title={`${error.status} ${error.statusText}`}>
            <p>{error.data}</p>
            <p>Back to <Link to='/'>safety</Link></p>
          </ErrorSection>
        </main>
      </Document>
    );
  }

  if (error instanceof Error) {
    return (
      <Document>
        <main>
          <ErrorSection title={`${error.message}`}>
            <p>{error.message}: {error.stack}</p>
            <p>Back to <Link to='/'>safety</Link></p>
          </ErrorSection>
        </main>
      </Document>
    );
  } else {
    return <h1>Unknown Error</h1>;
  }
}
